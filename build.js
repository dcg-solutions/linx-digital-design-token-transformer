#!/usr/bin/env node
/* eslint-disable @typescript-eslint/no-var-requires */
const nodeFetch = require('sync-fetch')
const rimraf = require('rimraf');
const _ = require('lodash');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const deepMerge = require('deepmerge');
const StyleDictionary = require('style-dictionary');
const fs = require('fs');
const webConfig = require('./libs/web');
const { getColorTokens } = require('./parsers/colors');
const { getFontSizeTokens } = require('./parsers/fontSize');
const { getFontWeightTokens } = require('./parsers/fontWeight');
const { getLineHeightTokens } = require('./parsers/lineheight');
const { getRadiusTokens } = require('./parsers/radius');

const argv = yargs(hideBin(process.argv)).argv;

const generateTypescriptTokens = _.template(
  fs.readFileSync(`${__dirname}/templates/nx-ts-tokens.template`)
);

const tsTemplate = _.template(
  fs.readFileSync(`${__dirname}/templates/custom-ts.template`)
);

const sortAsc = (a, b) => a - b;
const fetchWebFontData = (data) => {
  // https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap
  let baseUrl = 'https://fonts.googleapis.com/css2?display=swap';
  const fontNames = Object.keys(data);

  if (fontNames.length === 0) {
    return '';
  }

  fontNames.forEach(fontName => {
    const weights = data[fontName] || [];

    if (weights.length == 0) {

      baseUrl += `&family=${fontName}`;
      return;
    }

    baseUrl += `&family=${fontName}:wght@${(data[fontName] || []).sort(sortAsc).join(';')}`;
  })

  try {
    console.log('baseUrl', baseUrl);
    const response = nodeFetch(baseUrl);
    const data = response.text();

    // console.log('data', data);

    return data;
  } catch (er) {
    console.log("error", er);

    return '';
  }
}

StyleDictionary.registerFormat({
  name: 'nx-ts-tokens',
  formatter: (props) => {

    const colors = getColorTokens(props.allProperties);
    const fontSize = getFontSizeTokens(props.allProperties);
    const fontWeight = getFontWeightTokens(props.allProperties);
    const lineHeight = getLineHeightTokens(props.allProperties);
    const radius = getRadiusTokens(props.allProperties);
    
    return generateTypescriptTokens({
      colors: colors.keys,
      fontSize: fontSize.keys,
      fontWeight: fontWeight.keys,
      lineHeight: lineHeight.keys,
      radius
    })
  },
});

StyleDictionary.registerFormat({
  name: 'custom-ts',
  formatter: tsTemplate,
});

StyleDictionary.registerFormat({
  name: 'web-fonts',
  formatter: ({ allProperties }) => {
    // console.log('allProperties', allProperties);
    let fonts = {};

    allProperties.forEach(prop => {
      // console.log('prop', prop);
      if (!prop || !prop.original || typeof prop.original.value !== 'object') {
        return;
      }

      const { fontFamily, fontWeight } = prop.original.value;

      fonts[fontFamily] = fonts[fontFamily] || [];

      if (fonts[fontFamily].indexOf(fontWeight) === -1) {
        fonts[fontFamily].push(fontWeight);
      }

    });

    const data = fetchWebFontData(fonts);

    return data;
  },
});

// PATHS
const basePath = argv.basePath || './';
const buildPath = argv.distPath || './dist';
const srcPath = argv.srcPath || './input/*.json';

const StyleDictionaryExtended = StyleDictionary.extend({
  // adding imported configs
  ...deepMerge.all([webConfig]),
  source: [srcPath],
  platforms: {
    css: {
      transformGroup: 'custom/css',
      buildPath: buildPath + '/css/',
      files: [
        {
          filter: require('./libs/web/filterWeb'),
          format: 'custom/css',
          destination: 'variables.css',
        },
        {
          filter: require('./libs/web/filterFontWeb'),
          format: 'web-fonts',
          destination: 'fonts.css',
        }
      ],
    },
    // scss: {
    //   transformGroup: 'custom/css',
    //   buildPath: buildPath + '/scss/',
    //   files: [
    //     {
    //       filter: require('./libs/web/filterWeb'),
    //       format: 'scss/variables',
    //       destination: 'variables.scss',
    //     },
    //   ],
    // },
    // 'json-flat': {
    //   transformGroup: 'js',
    //   buildPath: buildPath + '/json/',
    //   files: [
    //     {
    //       filter: require('./libs/web/filterWeb'),
    //       format: 'json/flat',
    //       destination: 'tokens.json',
    //     },
    //   ],
    // },
    ts: {
      transformGroup: 'js',
      buildPath: buildPath + '/ts/',
      files: [
        // {
        //   filter: require('./libs/web/filterWeb'),
        //   format: 'custom-ts',
        //   destination: 'tokens.ts',
        // },
        {
          filter: require('./libs/web/filterWeb'),
          format: 'nx-ts-tokens',
          destination: 'tokens.ts',
        }
      ],
    },
  },
});

try {
  rimraf.sync(buildPath);
} catch (ex) {
  console.log('rimraf ex', ex);
}

StyleDictionaryExtended.buildAllPlatforms();
