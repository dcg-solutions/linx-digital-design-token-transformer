# [1.1.0](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/compare/v1.0.14...v1.1.0) (2022-01-14)


### Bug Fixes

* Ajusta ordem dos imports ([3e9f35d](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/3e9f35d4438a72ab4dc715f9ae1c7d704dda2b26))
* Ajusta retorno nas funções normalizer e colors ([1d189bf](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/1d189bf561eb53e7288ba696347791c2b7c424de))
* Altera keys_with_values por keysWithValues ([1ebacd0](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/1ebacd04b06227ab475e8147896028d84addd26a))
* Altera nome do output para tokens.ts e comenta output secundário com o mesmo nome ([880c67d](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/880c67dfa4b0437d99ebe639b39948cc478168a7))
* comenta output scss e json para configuração posterior ([dd7a69c](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/dd7a69c3858d21ce38b40bd3627f6eda7767a41f))
* Corrige imports do get nas funções parser ([62b76d7](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/62b76d70a3ec010e22fbd70405491b62585faaaf))


### Features

* Aprimora documentação do parser color ([b4e81ea](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/b4e81ea167c4cb44c09ef64ad351eaca23718c63))
* conecta função getColorTokens na montagem do css e remove códigos não utilizados ([a29ec9d](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/a29ec9d128dc564fa3b538114df82b10b851d202))
* conecta função normalizeTokenAndValue em colors ([014de63](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/014de633b24c3fc5c4071e4a74190e0dc225860a))
* Conecta nova função parseColorTokens no build ([5d0dab1](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/5d0dab18fe9b7ba7456dcba7733f00464750d37e))
* Conecta novos parsers na função formatCSS ([f8afa39](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/f8afa394626eb56a50551b6275fb56f742cda83e))
* Conecta novos parsers na montagem do arquivo .ts ([780efc2](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/780efc2daeaea6a8515fcc528feafddd8645b5ef))
* Conecta resultado dos novos parsers no template gerador do arquivo de types ts ([ae56383](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/ae56383128614acd5d6aa4b7d4c536cec9988cc3))
* Conecta sistema de radius na função de geração dos tokens para ts ([5d495e8](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/5d495e8c6c89b7c0e217668877d9dd2678f7a944))
* Cria função parseRadiusTokens ([6c1b65f](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/6c1b65fc7fa669acd9fe66e521f302a54e9dbe52))
* Cria novo template do styled-dictionary chamado nx-ts-tokens ([72af3fb](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/72af3fbb1a101783b8677a54034a45d206f22ed3))
* Implementa getRadiusTokensAndValues na montagem do arquivo css ([287575e](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/287575ecb478fdd2283387af0f87c10d540cecf8))
* implementa nova função getRadiusTokensAndValues  ([1ed9b4e](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/1ed9b4eaba8893d4c700a4d323a94b650b5826cc))
* Implementa script para gerar um tgz local ([5371a7e](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/5371a7e310f464539f220838115f6489a2be97bb))
* Monta função normalizer para os tokens ([22a29a3](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/22a29a3f33c606b3074db6db670c1397760dbe76))
* Monta função parseColorTokens para retornar um Array com as chaves tokens de cor ([a16c1f1](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/a16c1f1bde7cbbef5136e11c5e1e432078315b36))
* Monta nova função de geração de tokens no template nx-ts-tokens ([5a8dc94](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/5a8dc9497691f2bcd52759b5f497d8ae153d4a3b))
* nova função normalizeTokenAndValue ([9f804af](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/9f804af754bfcd54b4a105ce8a2c5e39117460c9))
* Novo parser para font-size ([4f63d63](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/4f63d633a198b22dd9ff12ae25751fe7b3602267))
* Novo parser para font-weight ([01a9328](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/01a93280448470b34c68707427625ddf666bc5d3))
* Novo parser para line-height ([44fa5bd](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/44fa5bdc6ecff90f31d2ee3897d11b9c30b6f719))
* Permite customizar a propriedade desejada na função normalizeTokenAndValue (parâmetro options) ([8a68af6](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/8a68af64781b7c2abd2ce2eba932f10792e50ab0))
* Permite options no parser do fontSize ([446671c](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/446671c3cb278bc60d55b30386957c0dc9c82f3a))
* Permite options no parser fontWeight ([36994cf](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/36994cfcc6f04ab015851eea19953ecd3cfb6bdd))
* Utiliza novo padrão da função getColorToken na build ([70e09a9](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/70e09a9327cf4b79fd3e371b6721bd9524355292))

## [1.0.14](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/compare/v1.0.13...v1.0.14) (2021-11-28)


### Bug Fixes

* remove arquivos de ci/cd do pacote tarball do npm ([ad87b47](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/ad87b47732add87ef9b453c16a5098a50b496133))

## [1.0.13](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/compare/v1.0.12...v1.0.13) (2021-11-28)


### Bug Fixes

* fix pipeline ([c062aa6](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/c062aa6d1be1d0d6eed53ff6315f0c9456a117a4))
* muda referencia da branch master para main no .releaserc ([c7b9584](https://bitbucket.org/dcg-solutions/linx-digital-design-token-transformer/commits/c7b958483f26e4145aaa3b3f032f9d0151210dfb))
