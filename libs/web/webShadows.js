const TinyColor = require('@ctrl/tinycolor');

module.exports = {
  type: 'value',
  matcher: function (token) {
    return token.type === 'custom-shadow' && token.value !== 0;
  },
  transformer: function ({ value }) {
    return `${value.shadowType === 'innerShadow' ? 'inset ' : ''}${
      value.offsetX || 0
    }px ${value.offsetY || 0}px ${value.radius || 0}px ${
      value.spread || 0
    }px ${new TinyColor.TinyColor(value.color).toRgbString()}`;
  },
};
