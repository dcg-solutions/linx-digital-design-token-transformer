/**
 * normalizeToken removes any unwanted char from token
 * @param {Array} token Expect an Array of objects containing token info in its properties [0: string, 1: string, 2: string]
 * @returns an Array of strings with token names
 */
const normalizeToken = (token) => (
  token.map(path => (
    path.split(' ')
      .join('-')
      .replace(/[^A-Za-z0-9 -]/g, '')
  ))
    .join('-')
    .split('--')
    .join('-')
);

/**
 * normalizeTokenAndValue
 * @param {Array} path 
 * @param {string} value 
 * @returns
 * a string containing token and value combination: `--color-primary-1: #FFFFFF`
 * can be used to build a css file using css variables
 */
const normalizeTokenAndValue = (path, value) => {
  const normalizedToken = path.map(item => item.split(' ').join('-').replace(/[^A-Za-z0-9 -]/g, ''))
  const token = normalizedToken.join('-').split('--').join('-')
  return `--${token}: ${value};`
}

module.exports = { normalizeToken, normalizeTokenAndValue }