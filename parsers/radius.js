/**
 * getRadiusTokens 
 * @returns an Array of strings with token names
 */
const getRadiusTokens = () => ([
  'normal',
  'round',
  'circle'
]);

/**
 * getRadiusTokensAndValues 
 * @returns an Array of strings with token names and values
 */
const getRadiusTokensAndValues = () => ([
  '--radius-normal: 6px;',
  '--radius-round: 8px;',
  '--radius-circle: 99px;',
])

module.exports = {
  getRadiusTokens,
  getRadiusTokensAndValues
}
