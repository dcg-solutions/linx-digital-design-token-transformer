const { get } = require('lodash');

const { normalizeToken, normalizeTokenAndValue } = require('./normalizer');

/**
 * normalizeFontSizeTokens 
 * Normalize font-size tokens names removing not used/irrelevant chars
 * @param {Array} param0 an Array of Object { attributes: Object, type: string, path: Array<string> }
 * @returns an Array of strings containing tokens names
 */
const normalizeFontSizeTokens = (data) => (
  data
    .filter(
      ({ attributes, type }) => (
        type === 'custom-fontStyle' &&
        attributes?.type === 'font size'
      )
    )
    .map(
      ({ path }) => normalizeToken(path)
    )
);

/**
 * normalizeFontSizeTokensAndValues 
 * Normalize font-size tokens names removing not used/irrelevant chars
 * @param {Array} param0 an Array of Object { attributes: Object, type: string, path: Array<string> }
 * @param {Object} param1 an Object containing path and value properties { path: string, value: string }
 * @returns an Array containing tokens names and values in a CSS file format (--${name}: ${value};)
 */
const normalizeFontSizeTokensAndValues = (data, options) => (
  data
    .filter(
      ({ attributes, type }) => (
        type === 'custom-fontStyle' &&
        attributes?.type === 'font size'
      )
    )
    .map(
      entry => normalizeTokenAndValue(
        options?.path ? get(entry, options.path) : get(entry, 'path'),
        options?.value ? get(entry, options.value) : get(entry, 'value.fontSize')
      )
    )
)

/**
 * getFontSizeTokens
 * Search in param0 data for building font-size tokens
 * @param {Array} param0 an Array of Object { attributes: Object, type: string, path: Array<string> }
 * @param {Object} param1 an Object containing path and value properties { path: string, value: string }
 * @returns
 * An Object containing two properties: keys and key_with_values
 * keys property is an Array of strings containing tokens names
 * keysWithValues is an Array containing tokens names and values in a CSS file format (--${name}: ${value};)
 */
const getFontSizeTokens = (data, options) => {
  return {
    keys: normalizeFontSizeTokens(data),
    keysWithValues: normalizeFontSizeTokensAndValues(data, options)
  }
}

module.exports = {
  getFontSizeTokens,
  normalizeFontSizeTokens,
  normalizeFontSizeTokensAndValues
}

