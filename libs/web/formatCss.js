// const formattedVariables = require('./formattedVariables');
const fileHeader = require('./fileHeader');
const { getColorTokens } = require('../../parsers/colors');
const { getFontSizeTokens } = require('../../parsers/fontSize');
const { getFontWeightTokens } = require('../../parsers/fontWeight');
const { getLineHeightTokens } = require('../../parsers/lineheight');
const { getRadiusTokensAndValues } = require('../../parsers/radius');

// const filteredTokens = (dictionary, filterFn) => {
//   const filtered = dictionary.allTokens.filter((token) => filterFn(token));
//   return {
//     ...dictionary,
//     ...{
//       allProperties: filtered,
//       allTokens: filtered,
//     },
//   };
// };

module.exports = (props) => {
  const { dictionary, options, file } = props

  // const opts = options || {};
  // const { outputReferences } = opts;
  // const groupedTokens = {
  //   // if you export the prefixes use token.path[0] instead of [1]
  //   light: filteredTokens(
  //     dictionary,
  //     (token) => token.path[1].toLowerCase() === 'light',
  //   ),
  //   dark: filteredTokens(
  //     dictionary,
  //     (token) => token.path[1].toLowerCase() === 'dark',
  //   ),
  //   rest: filteredTokens(
  //     dictionary,
  //     (token) => !['light', 'dark'].includes(token.path[1].toLowerCase()),
  //   ),
  // };

  const colorTokens = getColorTokens(dictionary.allProperties)
  const fontSizeTokens = getFontSizeTokens(dictionary.allProperties, { value: 'original.value.fontSize' })
  const fontWeightTokens = getFontWeightTokens(dictionary.allProperties, { value: 'original.value.fontWeight' })
  const lineHeightTokens = getLineHeightTokens(dictionary.allProperties, { value: 'original.value.lineHeight' })
  const radiusTokens = getRadiusTokensAndValues()

  return (
    fileHeader({ file }) +
    ':root {\n' +
    colorTokens.keysWithValues.join('\n') + '\n' +
    fontSizeTokens.keysWithValues.join('\n') + '\n' +
    fontWeightTokens.keysWithValues.join('\n') + '\n' +
    lineHeightTokens.keysWithValues.join('\n') + '\n' +
    radiusTokens.join('\n') +
    '\n}\n\n'
  );
};
