const { get } = require('lodash');

const { normalizeToken, normalizeTokenAndValue } = require('./normalizer');

/**
 * normalizeLineHeightTokens 
 * Normalize line-height tokens names removing not used/irrelevant chars
 * @param {Array} param0 an Array of Object { attributes: Object, type: string, path: Array<string> }
 * @returns an Array of strings containing tokens names
 */
const normalizeLineHeightTokens = (data) => (
  data
    .filter(
      ({ attributes, type }) => (
        type === 'custom-fontStyle' &&
        attributes?.type === 'line height'
      )
    )
    .map(
      ({ path }) => normalizeToken(path)
    )
);

/**
 * normalizeLineHeightTokensAndValues 
 * Normalize line-height tokens names removing not used/irrelevant chars
 * @param {Array} param0 an Array of Object { attributes: Object, type: string, path: Array<string> }
 * @param {Object} param1 an Object containing path and value properties { path: string, value: string }
 * @returns an Array containing tokens names and values in a CSS file format (--${name}: ${value};)
 */
const normalizeLineHeightTokensAndValues = (data, options) => (
  data
    .filter(
      ({ attributes, type }) => (
        type === 'custom-fontStyle' &&
        attributes?.type === 'line height'
      )
    )
    .map(
      entry => normalizeTokenAndValue(
        options?.path ? get(entry, options.path) : get(entry, 'path'),
        options?.value ? get(entry, options.value) : get(entry, 'value.lineHeight')
      )
    )
)

/**
 * getLineHeightTokens
 * Search in param0 data for building line-height tokens
 * @param {Array} param0 an Array of Object { attributes: Object, type: string, path: Array<string> }
 * @param {Object} param1 an Object containing path and value properties { path: string, value: string }
 * @returns
 * An Object containing two properties: keys and key_with_values
 * keys property is an Array of strings containing tokens names
 * keysWithValues is an Array containing tokens names and values in a CSS file format (--${name}: ${value};)
 */
const getLineHeightTokens = (data, options) => {
  return {
    keys: normalizeLineHeightTokens(data),
    keysWithValues: normalizeLineHeightTokensAndValues(data, options)
  }
}

module.exports = {
  getLineHeightTokens,
  normalizeLineHeightTokens,
  normalizeLineHeightTokensAndValues
}
