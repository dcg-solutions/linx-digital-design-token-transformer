const { normalizeToken, normalizeTokenAndValue } = require('./normalizer');

/**
 * parseColorTokens 
 * @param {Array} param0 an Array of Object { type: string, path: Array<string> }
 * @returns an Array of strings containing tokens names
 */
const normalizeColorTokens = (data) => (
  data
    .filter(({ type }) => type === 'color')
    .map(({ path }) => normalizeToken(path))
);

/**
 * normalizeColorTokensAndValue 
 * Normalize color tokens names removing not used/irrelevant chars
 * @param {Array} param0 an Array of Object { type: string, path: Array<string> }
 * @returns an Array containing tokens names and values in a CSS file format (--${name}: ${value};)
 */
const normalizeColorTokensAndValue = (data) => (
  data
    .filter(({ type }) => type === 'color')
    .map(entry => normalizeTokenAndValue(entry.path, entry.value))
)

/**
 * getColorTokens
 * Search in param0 data for building line-height tokens
 * @param {Array} param0 an Array of Object { type: string, path: Array<string> }
 * @returns
 * An Object containing two properties: keys and key_with_values
 * keys property is an Array of strings containing tokens names
 * keysWithValues is an Array containing tokens names and values in a CSS file format (--${name}: ${value};)
 */
const getColorTokens = (data) => {
  return {
    keys: normalizeColorTokens(data),
    keysWithValues: normalizeColorTokensAndValue(data)
  }
}

module.exports = {
  getColorTokens,
  normalizeColorTokens,
  normalizeColorTokensAndValue
}
